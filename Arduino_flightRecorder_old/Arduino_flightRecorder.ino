
/*
 * Arduino Flight Controller
 * -------------------------
 * Flight recorder and controller for the Whiskey Tango Foxtrot amateur rocket
 * Authors : Antoine Jeanson
 * TODO :
 * - Modify the code as to avoid using String to generate the data string, as this can cause memory fragmentation.
 *   This can also cause performance issues
 */

// NOTE : temperature correction for MPU6050 temp°C = Tmp/340.00+36.53

// Librairies
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <BMP280_DEV.h>

// MPU6050 settings and variables
const int MPU_addr = 0x68;  // I2C address of the MPU-6050
int16_t AcX,AcY,AcZ,MPU_Tmp,GyX,GyY,GyZ; // 16 bit integers for the MPU outputs
float MPU_Tmp_corr,AcX_corr,AcY_corr,AcZ_corr,GyX_corr,GyY_corr,GyZ_corr;

BMP280_DEV bmp280;  // Set up I2C communications
const int BMP_addr = 0x75; // I2C address of the BMP-280
float pres, BMP_Tmp;

// SD Reader settings
const int chipSelect = 4; // Arduino pin connected to CS

const int sd_enable = 5;

bool sd_enabled;

File dataFile;

void setup() {
  pinMode(sd_enable, INPUT_PULLUP);
  
  Serial.begin(115200);
    while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
    }
  // SD Card initialization
  Serial.print("Initializing SD card...");
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("card initialized.");
  sd_enabled = true;

  // Initialize I2C devices
  Wire.begin();
  Serial.println("Initializing MPU-6050");
  init_MPU();
  Serial.println("Done.");
  Serial.println("Initializing BMP-280");
  init_BMP();
  Serial.println("Done.");
}

void loop() {
  // get MPU values
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false); // Send restart
  Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers, send stop
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  MPU_Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H)   & 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H)  & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H)  & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H)  & 0x48 (GYRO_ZOUT_L)
  
  // Correct them. Note : this creates uncertainties in the numbers. Only raw values should be stored, not corrected values. They are fine for onboard use though.
  MPU_Tmp_corr = MPU_Tmp/340.00+36.53;
  AcX_corr = (float(AcX)/(32768))*16*9.80665;
  AcY_corr = (float(AcY)/(32768))*16*9.80665;
  AcZ_corr = (float(AcZ)/(32768))*16*9.80665;
  GyX_corr = (float(GyX)/(32768))*2000;
  GyY_corr = (float(GyY)/(32768))*2000;
  GyZ_corr = (float(GyZ)/(32768))*2000;

  // Get BMP values, they are already in the correct format
  bmp280.getTempPres(BMP_Tmp,pres);

  // Generate the CSV string
  String dataString = String(micros()) + "," + String(AcX_corr,8) + "," + String(AcY_corr,8) + "," + String(AcZ_corr,8) + "," + String(GyX_corr,8) + "," + String(GyY_corr,8) + "," + String(GyZ_corr,8) + "," + String(pres*100) + "," + String(MPU_Tmp_corr) + "," + String(BMP_Tmp);
  //dataString = String(pres*1000);
  Serial.println(dataString);
  Serial.println(digitalRead(sd_enable));
  if (digitalRead(sd_enable) == LOW && sd_enabled == true) {
    write_to_SD(dataString);
  } else if (digitalRead(sd_enable) == HIGH) {
    sd_enabled = false;
  } else if (digitalRead(sd_enable) == LOW && sd_enabled == false) {
      Serial.print("Initializing SD card...");
    if (!SD.begin(chipSelect)) {
      Serial.println("Card failed, or not present");
      // don't do anything more:
      return;
    } else {
      sd_enabled = true;
    }
  }
  delay(0);
}

void init_MPU() {
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);      // PWR_MGMT_1 register
  Wire.write(0);         // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x1B);      // Gyro range setting register
  Wire.write(B00011000); // Set to ±2000°/S
  Wire.endTransmission(true);
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x1C);      // Accel range setting register
  Wire.write(B00011000); // Set to ±16g
  Wire.endTransmission(true);
}

void init_BMP() {
  bmp280.begin(BMP280_I2C_ALT_ADDR);
  bmp280.setPresOversampling(OVERSAMPLING_X16);
  bmp280.setTempOversampling(OVERSAMPLING_X2);
  bmp280.setIIRFilter(IIR_FILTER_2);
  bmp280.setTimeStandby(TIME_STANDBY_62MS);
  bmp280.startNormalConversion();
}

void write_to_SD(String dataString) {
  File dataFile = SD.open("data.csv", FILE_WRITE); // Open file on SD card
  if (dataFile) { // If the file is available
    dataFile.println(dataString); // Print the string
  }
  else { // Debug, when something goes wrong
    Serial.println("Error writing to file");
  }
  dataFile.close(); // Closes the file, saving its contents
}
