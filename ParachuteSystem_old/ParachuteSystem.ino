
/*
 * Parachute System code
 * ----------------------
 * Designed for the Whiskey Tango Foxtrot amateur rocket
 * Authors : Eva Rangayen
 */

#include <Servo.h>

Servo servopara; // create servo object to control a servo named servopara
const int PIN_SERVO_PARACHUTE = 9;
const int PIN_LED_STATUS = 3;
const int PIN_LIFTOFF_SENSOR = 10;
const int PIN_SETTING_SWITCH = 11;

const int servo_close = 180;
const int servo_open = 70;

const float Target_apogee_time = 7.2 * 1000000; // in microseconds

unsigned long currentTime;
unsigned long liftoffTime;
unsigned long flightTime;

bool liftoff_detected = false;

bool forced_open_mode = false;

void setup() {
  Serial.begin(115200);
  Serial.println("Booting up !");

  pinMode(PIN_LED_STATUS,OUTPUT);
  pinMode(PIN_SERVO_PARACHUTE,OUTPUT);

  pinMode(PIN_LIFTOFF_SENSOR,INPUT_PULLUP);
  pinMode(PIN_SETTING_SWITCH, INPUT_PULLUP);

  servopara.attach(9); // attach the servo on pin 9 to the servo object
  servopara.write(servo_close); // tell servo to go to position 'closed'
  delay(15);  // Wait for the servo to get to the required position

  if (digitalRead(PIN_SETTING_SWITCH) == LOW) {
    forced_open_mode = true;
    Serial.println("Init : manual control");
  }
  else {
    Serial.println("Init : Sequencer mode");
    forced_open_mode = false;
    while (digitalRead(PIN_LIFTOFF_SENSOR) == LOW) {
      // Do nothing, wait for jack to be disconnected.
    }
    Serial.println("Init : liftoff detected !");
    liftoffTime = micros(); // Remember the time when we left the loop
  }
}

void loop() {
  if (forced_open_mode == false) {
    // Sequencer mode
    currentTime = micros();
    flightTime = currentTime - liftoffTime;
    Serial.println(flightTime);

    if (flightTime >= Target_apogee_time){
      servopara.write(servo_open); // tell servo to go to position 'open'
    }

  }
  else {
    // Manual control mode
    if (digitalRead(PIN_SETTING_SWITCH) == LOW) {
      Serial.println("Manual control : opening door.");
      servopara.write(servo_open);
    }
    else {
      Serial.println("Manual control : closing door.");
      servopara.write(servo_close);
    }
  }
  delay(100);
}
